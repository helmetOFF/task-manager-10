package ru.helmetoff.tm.controller;

import ru.helmetoff.tm.api.ICommandController;
import ru.helmetoff.tm.api.ICommandService;
import ru.helmetoff.tm.constant.IApplicationInfo;
import ru.helmetoff.tm.model.TerminalCommand;
import ru.helmetoff.tm.util.INumberUtil;

import java.util.Arrays;

public class CommandController implements ICommandController {

    private ICommandService CommandService;

    public CommandController(ICommandService CommandService) {
        this.CommandService = CommandService;
    }

    public void displayHelp() {
        System.out.println("[HELP]");
        final TerminalCommand[] commands = CommandService.getTerminalCommands();
        for (final TerminalCommand command: commands)
            System.out.println(command);
    }

    public void displayCommands() {
        final String[] commands = CommandService.getCommands();
        System.out.println(Arrays.toString(commands));
    }

    public void displayArguments() {
        final String[] arguments = CommandService.getArguments();
        System.out.println(Arrays.toString(arguments));
    }

    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println(IApplicationInfo.VERSION);
    }

    public void displayAbout() {
        System.out.println("ABOUT");
        System.out.println("NAME: " + IApplicationInfo.DEVELOPER_NAME);
        System.out.println("E-MAIL: " + IApplicationInfo.DEVELOPER_EMAIL);
    }

    public void displayInfo() {
        System.out.println("[INFO]");

        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);

        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryFormat = INumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);

        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = INumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = (maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue);
        System.out.println("Maximum memory: " + maxMemoryFormat);

        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryFormat = INumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);

        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + INumberUtil.formatBytes(usedMemory));

    }

    public void displayWrongCommand(final String arg) {
        System.out.println(String.format("\"%s\" is invalid command."));
    }

    public void exit() {
        System.exit(0);
    }

}

package ru.helmetoff.tm.api;

import ru.helmetoff.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArguments();

}

package ru.helmetoff.tm.constant;

public interface ITerminalConst {

    String HELP = "help";

    String HELP_DESCRIPTION = "Display list of terminal commands.";

    String VERSION = "version";

    String VERSION_DESCRIPTION = "Display program version.";

    String ABOUT = "about";

    String ABOUT_DESCRIPTION = "Display developer info.";

    String EXIT = "exit";

    String EXIT_DESCRIPTION = "Close Application.";

    String INFO = "info";

    String INFO_DESCRIPTION = "Display information about system.";

    String COMMANDS = "commands";

    String COMMANDS_DESCRIPTION = "Display list of commands.";

    String ARGUMENTS = "arguments";

    String ARGUMENTS_DESCRIPTION = "Display list of arguments.";

}

package ru.helmetoff.tm.bootstrap;

import ru.helmetoff.tm.api.ICommandController;
import ru.helmetoff.tm.api.ICommandRepository;
import ru.helmetoff.tm.api.ICommandService;
import ru.helmetoff.tm.constant.IArgumentConst;
import ru.helmetoff.tm.constant.ITerminalConst;
import ru.helmetoff.tm.controller.CommandController;
import ru.helmetoff.tm.repository.CommandRepository;
import ru.helmetoff.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(String... args) {
        displayWelcome();
        if (parseArgs(args)) System.exit(0);
        while (true) {
            final Scanner scanner = new Scanner(System.in);
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case ITerminalConst.HELP:
                commandController.displayHelp();
                break;
            case ITerminalConst.VERSION:
                commandController.displayVersion();
                break;
            case ITerminalConst.ABOUT:
                commandController.displayAbout();
                break;
            case ITerminalConst.INFO:
                commandController.displayInfo();
                break;
            case ITerminalConst.EXIT:
                commandController.exit();
                break;
            case ITerminalConst.COMMANDS:
                commandController.displayCommands();
                break;
            case ITerminalConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            default:
                commandController.displayWrongCommand(command);
                break;
        }
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.HELP:
                commandController.displayHelp();
                break;
            case IArgumentConst.VERSION:
                commandController.displayVersion();
                break;
            case IArgumentConst.ABOUT:
                commandController.displayAbout();
                break;
            case IArgumentConst.INFO:
                commandController.displayInfo();
                break;
            case IArgumentConst.COMMANDS:
                commandController.displayCommands();
                break;
            case IArgumentConst.ARGUMENTS:
                commandController.displayArguments();
                break;
            default:
                commandController.displayWrongCommand(arg);
                break;
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

}
